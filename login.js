import React, { Component } from 'react';
import axios from "axios";
import style from 'styled-components';

let LoginContaner = style.div
`
margin-top:100px;
background-color:#e6e6e6;
height:300px;
padding:40px;
`

let Title = style.h1
` display: flex;
justify-content: center;
margin-top:70px;
`

class Login extends Component {
    constructor() {
        super()
        this.state = {
            email: '',
            password: ''
        }

    }
    changeEmail(e) {
        this.setState({
            email: e.target.value
        })
    }

    changePassword(e) {
        this.setState({
            password: e.target.value
        })
    }
    Login(e) {
        e.preventDefault();
        const data = {
             email: this.state.email,
             password: this.state.password,
           }

           
         axios.post("http://localhost:5000/api/user/login", data )
         .then(res => {
            console.log(res.data);
             if(res.data==true){
             
                localStorage.setItem('token',true)
                document.location.reload()

             }
             else{
                 alert("you are not admin")
             }
         
         }).catch(error=>{
            alert(error)
         }
            
         )}
     render() {
        return (
            <div>
                <Title>Admin Login</Title>
            <LoginContaner className="container">
                <form encType="multipart/form-data" onSubmit={this.Login.bind(this)} method="POST">
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Email address</label>
                        <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email" value={this.state.email} onChange={this.changeEmail.bind(this)} />
                        <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group">
                        <label htmlFor="exampleInputPassword1">Password</label>
                        <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" name="password" value={this.state.password} onChange={this.changePassword.bind(this)} />
                    </div>


                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </LoginContaner>
            </div>
        );
    }
}

export default Login;