import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link, DefaultRoute } from 'react-router-dom';
import UsarDashbord from './components/userdashbord';
import Ads from './components/adsdashbord'
import Header from './components/header'
import Login from './login'


class App extends React.Component {
    constructor() {
        super()
        this.state={
            token:localStorage.getItem("token")
        }
    }
    render() {
        return (
            <div>
                {this.state.token=="true" ?
                  <BrowserRouter>
                  <div>
                      <Header />
                      <Route exact path="/" component={UsarDashbord} />
                      <Route path="/ads" component={Ads} />
                  </div>
              </BrowserRouter>: <Login />
             }
            </div>
        )
    }
}


ReactDOM.render(<App />, document.getElementById('root'))