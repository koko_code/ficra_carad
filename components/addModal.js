import React from 'react';
import axios from 'axios';
import { Pane, Button, Dialog, TextInput, FilePicker, toaster } from 'evergreen-ui'


export default class AddModal extends React.Component{
    constructor(){
        super()
        this.state={
            isShown:false,
            adTitle:"",
            adOwner:"",
            repetition:"",
            video:null
        }
    }
    
    insertAd(){
        const data = new FormData()
        data.append('file', this.state.video, this.state.video.name)
        data.append('title', this.state.adTitle)
        data.append('owner', this.state.adOwner)
        data.append('repeat', this.state.repetition)

        axios.post("http://localhost:5000/api/ad", data)
        .then(res => {
        console.log("Data was Uploaded !")
        this.setState({ isShown: false, adTitle:"", adOwner:"", repetition:"", video:null });
        toaster.success('Your Data was Uploaded !')
        })

    }

    render() {
      return (
        <div>
                    <Pane>
                        <Dialog
                            isShown={this.state.isShown}
                            title="Insret a new Ad"
                            onCloseComplete={() => this.setState({ isShown: false })}
                            confirmLabel="add ad"
                            onConfirm={() => this.insertAd()}
                        >
                            Ad Title<br/>
                            <TextInput 
                             placeholder="Ad Title" 
                             width="90%"
                             value={this.state.adTitle} 
                             onChange={(event)=>{this.setState({adTitle: event.target.value})}}
                            /><br/><br/>

                             
                            Ad Owner<br/>
                            <TextInput 
                            width="90%"
                            placeholder="Ad Owner" 
                            value={this.state.adOwner} 
                            onChange={(event)=>{this.setState({adOwner: event.target.value})}} 
                            /><br/><br/> 

                            
                            How Many time should this ad be repeated ?<br/>
                            <TextInput 
                            width="90%"
                            placeholder="Ad repetition" 
                            value={this.state.repetition} 
                            onChange={(event)=>{this.setState({repetition: event.target.value})}} 
                            /><br/><br/>

                             
                            Ad Video<br/>
                            <FilePicker width={250} marginBottom={32} width="90%" onChange={files => this.setState({video:files[0]})}/>
                        </Dialog>

                        <Button onClick={() => this.setState({ isShown: true })}>New Ad</Button>
                    </Pane>
            </div>

      )
    }
}


