import React from 'react';
import axios from 'axios';
import AddModal from './addModal'
import { Pane, Heading} from 'evergreen-ui'

const colorStyle = {
    color: "#F9F9FB",
    margin: "10px",
    fontSize: "30px"
};

export default class AdsHeader extends React.Component {
    constructor() {
        super()
        this.state= {}
    }

    render(){
        return (
            <div>
                <Pane display="flex" padding={16} background="overlay" borderRadius={0}>

                    <Pane flex={1} alignItems="center" display="flex">
                        <Heading size={500} style={colorStyle} >Ads</Heading>
                    </Pane>
                    <AddModal/>
                </Pane>
            </div>
        );
    }
}

