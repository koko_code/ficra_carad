import React from 'react';
import style from 'styled-components';
import { Pane, Dialog, Button, Table, TextInput, Heading } from 'evergreen-ui';
import axios from 'axios'


const colorStyle = {
    color: "#F9F9FB",
    margin: "10px",
    fontSize: "30px"
};

let TableContaner = style.div
    `
margin: 10px 3%;

`


class UsarDashbord extends React.Component {

    constructor() {
        super()
        this.state = {
            isShown: false,
            isLoading: false,
            name: '',
            email: '',
            password: '',
            users: [],
        }

    };


    componentDidMount() {
        axios.get(`http://localhost:5000/api/user/`)
            .then(res => {
                const users = res.data;
                this.setState({ users });
            })
    };

    componentDidUpdate() {
        axios.get(`http://localhost:5000/api/user/`)
            .then(res => {
                const users = res.data;
                this.setState({ users });
            })
    }

    delete(email) {
        console.log("hello from delete")
        axios.delete(`http://localhost:5000/api/user/delet/${email}`)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
    }

    post() {
        axios.post(`http://localhost:5000/api/user/register`, { name: this.state.name, email: this.state.email, password: this.state.password })
            .then(res => {
                console.log(res);
                console.log(res.data);
                this.setState({ name: '', email: '', password: '' })
            })
    }

    render() {
        return (
            <div>

                <Pane display="flex" padding={16} background="overlay" borderRadius={0}>

                    <Pane flex={1} alignItems="center" display="flex">
                        <Heading size={600} style={colorStyle} >Users</Heading>
                    </Pane>

                    <Pane>
                        <Dialog
                            isShown={this.state.isShown}
                            title="Add New User"
                            onCloseComplete={() => this.setState({ isShown: false })}
                            confirmLabel="Add user"
                            onConfirm={() => { this.post(); this.setState({ isShown: false }); }}
                        >
                            <TextInput
                                placeholder="User Name"
                                onChange={e => this.setState({ name: e.target.value })}
                                value={this.state.name}
                            />
                            <TextInput
                                placeholder="E-mail"
                                onChange={e => this.setState({ email: e.target.value })}
                                value={this.state.email}
                            />
                            <TextInput
                                placeholder="PassWord"
                                onChange={e => this.setState({ password: e.target.value })}
                                value={this.state.password}
                            />
                        </Dialog>

                        <Button onClick={() => this.setState({ isShown: true })}>Add New User</Button>
                    </Pane>

                </Pane>

                <TableContaner   >
                    <Table>
                        <Table.Head>
                            <Table.TextHeaderCell>
                                User Name
                            </Table.TextHeaderCell>
                            <Table.TextHeaderCell>
                                E-mail
                            </Table.TextHeaderCell>
                            <Table.TextHeaderCell></Table.TextHeaderCell>
                        </Table.Head>
                        <Table.Body height={500}>
                            {this.state.users.map(users => (
                                <Table.Row>
                                    <Table.TextCell>{users.name}</Table.TextCell>
                                    <Table.TextCell>{users.email}</Table.TextCell>
                                    <Table.TextCell>
                                        <Button iconBefore="trash" intent="danger"
                                            onClick={() => { this.delete(users.email); }}
                                        >
                                            Delete User
                                    </Button>
                                    </Table.TextCell>
                                </Table.Row>
                            ))}
                        </Table.Body>
                    </Table>
                </TableContaner>
            </div>
        )
    }
}



export default UsarDashbord;


