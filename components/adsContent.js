import React from 'react';
import axios from 'axios';
import EditModal from './editModal'
import { Text, Pane, Button, Heading, IconButton, Link, Strong, toaster} from 'evergreen-ui'

const whiteBackground ={
    backgroundColor: "#FFFFFF"
}

export default class AdsContent extends React.Component{
    constructor(){
        super()
        this.state={
            ads:[]
        }
    
    }

    componentDidMount(){
        this.getAds();
    }
    componentDidUpdate(){
        this.getAds();
    }
    getAds(){
        axios.get(`http://localhost:5000/api/ad`)
        .then(res => {
          const ads = res.data;
          this.setState({ ads });
        })        
    }

    deleteAd(adId){
        if (confirm("Are You Sure You Want To Delete This Ad ?")) {
            axios.delete(`http://localhost:5000/api/ad/${adId}`, {
                params: {
                    id: adId
                }
            }).then(function (response) {
                toaster.success('Your data was deleted')
            }).catch((err)=>{
                console.log(err)
            })
        }
    }

    render(){
        return(
            <Pane clearfix background="tint2" display="flex" justifyContent="center" alignItems="center" flexDirection="column"> 
            {
                this.state.ads.map((ad, i)=>{
                    return <Pane
                            key={i}
                            elevation={3}
                            float="left"
                            width="90%"
                            height={200}
                            marginY={24}
                            marginTop={24}
                            paddingX={30}
                            display="flex"
                            flexDirection="row"
                            style={whiteBackground}
                            >
                                <Pane width="90%" display="flex" flexDirection="column">
                                    <Heading size={900} marginTop={20}>{ad.title}</Heading>
                                    <Text size={300} marginBottom="20px">ID : {ad._id}</Text>
                                    <Text ><Strong>Owner : </Strong>{ad.adOwner}</Text>
                                    <Text ><Strong>Repeat : </Strong>{ad.repetition} </Text>
                                    <Text ><Strong>Counter : </Strong>{ad.counter} </Text>
                                    <Link target="_blank" href={"http://localhost:5000/"+ad.video} marginRight={12}>View video</Link>
                                </Pane>
                                <Pane display="flex" width="10%" flexDirection="row" justifyContent="space-around" alignItems="flex-end" marginBottom="24px">
                                    <IconButton icon="edit" color="muted" appearance="minimal" height={50} data-toggle="modal" data-target={"#editModal"+ad._id}/>
                                    <EditModal EditingAd={ad}/>
                                    <IconButton icon="delete" intent="danger" appearance="minimal" height={50} onClick={()=>this.deleteAd(ad._id)}/>
                                </Pane>
                            </Pane>
                })
            }        
            </Pane>
        )
    }
}

