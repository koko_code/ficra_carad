import React from 'react';
import AdsHeader from './adsHeader.js'
import AdsContent from './adsContent'


export default class Ads extends React.Component {
    render(){
        return(
            <div>
                <AdsHeader/>
                <AdsContent/>
            </div>
        )
    }
}
