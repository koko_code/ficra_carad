import React from 'react';
import axios from 'axios';
import { toaster } from 'evergreen-ui'


export default class EditModal extends React.Component{
    constructor(){
        super();
        this.state={
            _id: "",
            title: "",
            adOwner: "",
            repetition: 0,
            counter: 0,
            video: null
        }
    }

    componentDidMount(){
        this.setState({
            _id: this.props.EditingAd._id,
            title: this.props.EditingAd.title,
            adOwner: this.props.EditingAd.adOwner,
            repetition: this.props.EditingAd.repetition,
            counter: this.props.EditingAd.counter,
            video: this.props.EditingAd.video
        })

    }

    editAd(e){
        e.preventDefault()

        const data ={
            _id: this.state._id,
            title: this.state.title,
            adOwner: this.state.adOwner,
            repetition: this.state.repetition,
            counter: this.state.counter,
            video: this.state.video
        }

        axios.put(`http://localhost:5000/api/ad/${this.state._id}`, {ad: data})
        .then(function (response) {
            toaster.success('Data was Updated :)')
        }).catch((err)=>{
            console.log(err)
        })
    }

    render() {
      return (
        <div class="modal fade" id={"editModal"+this.props.EditingAd._id} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit this Ad</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <form onSubmit={(e)=>this.editAd(e)}>
                    <div class="form-group">
                        <label >Ad Title</label>
                        <input value={this.state.title} onChange={(e)=>{this.setState({title:e.target.value})}} type="text" class="form-control" placeholder="Give this Ad a Title"></input>
                    </div>
                    <div class="form-group">
                        <label>Ad Owner</label>
                        <input value={this.state.adOwner} onChange={(e)=>{this.setState({adOwner:e.target.value})}} type="text" class="form-control" placeholder="Who is the Owner of this Ad"></input>
                    </div>
                    <div class="form-group">
                        <label>Repetition</label>
                        <input value={this.state.repetition} onChange={(e)=>{this.setState({repetition:e.target.value})}} type="text" class="form-control" placeholder="How many times should this ad be repeated"></input>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-dark">Save</button>
                    </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
      )
    }
}


