import React from 'react';
import style from 'styled-components';
import { BrowserRouter, Route, Link, DefaultRoute } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Pane, Dialog, Button, Table, TextInput, Paragraph, Tablist, Tab } from 'evergreen-ui';


let NavBar = style.ul
    `
display: flex;
margin: 0px 10px;
position: absolute;
top: 10px;
right: 0;
`


class Header extends React.Component {
    constructor() {
        super();
        this.state = {
            selectedIndex: 0,
            tabs: ['Users', 'Ads']
        }
    }

    render() {
        return (
            <div>
                <AppBar position="static" color="default">
                    <Toolbar>
                        <Typography variant="h6" color="inherit">
                            Admin's Dashbord
                        </Typography>
                    </Toolbar>
                </AppBar>
                <NavBar>

                    <Link exact to="/" >
                    <Button appearance="minimal" height={40} >Users</Button>
                    </Link>
                    <Link to="/ads">
                    <Button appearance="minimal" height={40} >Ads</Button>
                    </Link>

                    <Button height={40} intent="danger" appearance="primary"
                        onClick={() => { localStorage.clear(); document.location.reload() }}
                    >Log Out</Button>
                </NavBar>
            </div >

        )
    }
}

export default Header;